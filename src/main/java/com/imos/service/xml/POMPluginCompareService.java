/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.service.xml;

import com.imos.xml.parser.PluginInPOMFileParser;
import com.imos.xml.parser.POMFileSAXParser;
import com.imos.model.Plugin;
import com.imos.view.model.PluginData;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.xml.sax.SAXException;

/**
 *
 * @author p
 */
@Log4j2
@ApplicationScoped
public class POMPluginCompareService implements Serializable {

    @Inject
    private POMFileSAXParser parser;

    @Inject
    private PluginInPOMFileParser pluginParser;

    @Getter
    private List<PluginData> pluginData;

    public String comparePOMFiles(String firstFileData, String secondFileData, boolean full) {
        StringBuilder builder = new StringBuilder();
        Set<String> allKeys = new HashSet<>();
        Map<String, Plugin> keyMapFileTwo = new HashMap<>();
        Map<String, Plugin> keyMapFileOne = new HashMap<>();
        try {
            parser.parsePOMXML(pluginParser, firstFileData);
            parser.getAllData().forEach((object) -> {
                Plugin plugin = (Plugin) object;
                keyMapFileOne.put(plugin.getGroupId() + ":" + plugin.getArtifactId(), plugin);
            });

            parser.parsePOMXML(pluginParser, secondFileData);
            parser.getAllData().forEach((object) -> {
                Plugin plugin = (Plugin) object;
                keyMapFileTwo.put(plugin.getGroupId() + ":" + plugin.getArtifactId(), plugin);
            });

            pluginParser.validateAllProperties();

            allKeys.addAll(keyMapFileOne.keySet());
            allKeys.addAll(keyMapFileTwo.keySet());
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            log.error(ex.getMessage());
        }
        pluginData = new ArrayList<>();
        parser.getAllProperties()
                .forEach((key, value) -> {
                    builder.append(key);
                    builder.append(" ");
                    builder.append(value);
                    builder.append(NEW_LINE_ONE);
                });
        builder.append(NEW_LINE_ONE);
        allKeys.forEach((key) -> {
            PluginData data = new PluginData();
            Plugin pluginFileOne = keyMapFileOne.get(key);
            Plugin pluginFileTwo = keyMapFileTwo.get(key);
            boolean valueAdded = false;
            if (pluginFileOne == null && pluginFileTwo != null) {
                valueAdded = true;
                data.setVersionOne("Dependency NA");
                data.setVersionTwo(pluginFileTwo.getVersion());
            } else if (pluginFileOne != null && pluginFileTwo == null) {
                valueAdded = true;
                data.setVersionOne(pluginFileOne.getVersion());
                data.setVersionTwo("Dependency NA");
            } else if (pluginFileOne != null && pluginFileTwo != null
                    && !pluginFileOne.getVersion().equals(pluginFileTwo.getVersion())) {
                valueAdded = true;
                data.setVersionOne(pluginFileOne.getVersion());
                data.setVersionTwo(pluginFileTwo.getVersion());
            } else if (pluginFileOne != null && pluginFileTwo != null && full) {
                valueAdded = true;
                data.setVersionOne(pluginFileOne.getVersion());
                data.setVersionTwo(pluginFileTwo.getVersion());
            }
            if (valueAdded) {
                String[] value = key.split(":");
                if (!value[0].isEmpty()) {
                    data.setGroupId(value[0]);
                }
                if (!value[1].isEmpty()) {
                    data.setArtifactId(value[1]);
                }
                pluginData.add(data);
            }
        });
        return builder.toString().trim().isEmpty() ? "No Result" : builder.toString();
    }

    public String comparePOMFiles(String firstFileData, String secondFileData) {
        return comparePOMFiles(firstFileData, secondFileData, false);
    }
    private static final String NEW_LINE_ONE = "<br/>";
    private static final String NEW_LINE_TWO = "<br/><br/>";
}
