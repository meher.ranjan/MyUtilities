/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.service.xml;

import com.imos.xml.parser.POMFileSAXParser;
import com.imos.xml.parser.DependencyInPOMFileParser;
import com.imos.model.Dependency;
import com.imos.view.model.DependencyData;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.xml.sax.SAXException;

/**
 *
 * @author p
 */
@Log4j2
@ApplicationScoped
public class POMDependencyCompareService implements Serializable {

    @Inject
    private POMFileSAXParser parser;

    @Inject
    private DependencyInPOMFileParser dependencyParser;

    @Getter
    private List<DependencyData> dependencyData;

    public String comparePOMFiles(String firstFileData, String secondFileData, boolean full) {
        StringBuilder builder = new StringBuilder();
        Map<String, Dependency> keyMapFileOne = new HashMap<>();
        Map<String, Dependency> keyMapFileTwo = new HashMap<>();
        Set<String> allKeys = new TreeSet<>();

        try {
            calculateData(firstFileData, keyMapFileOne);
            calculateData(secondFileData, keyMapFileTwo);

            allKeys.addAll(keyMapFileOne.keySet());
            allKeys.addAll(keyMapFileTwo.keySet());

        } catch (IOException | ParserConfigurationException | SAXException ex) {
            log.error(ex.getMessage());
        }

        parser.getAllProperties()
                .forEach((key, value) -> {
                    builder.append(key);
                    builder.append(" ");
                    builder.append(value);
                    builder.append(NEW_LINE_ONE);
                });
        builder.append(NEW_LINE_ONE);

        dependencyData = new ArrayList<>();
        allKeys.forEach((key) -> {
            try {
                DependencyData data = new DependencyData();
                Dependency dependencyFileOne = keyMapFileOne.get(key);
                Dependency dependencyFileTwo = keyMapFileTwo.get(key);
                boolean valueAdded = false;
                if (dependencyFileOne == null && dependencyFileTwo != null) {
                    valueAdded = true;
                    data.setVersionOne("Dependency NA");
                    data.setVersionTwo(dependencyFileTwo.getVersion());
                } else if (dependencyFileOne != null && dependencyFileTwo == null) {
                    valueAdded = true;
                    data.setVersionOne(dependencyFileOne.getVersion());
                    data.setVersionTwo("Dependency NA");
                } else if (dependencyFileOne != null && dependencyFileTwo != null
                        && !dependencyFileOne.getVersion().equals(dependencyFileTwo.getVersion())) {
                    valueAdded = true;
                    data.setVersionOne(dependencyFileOne.getVersion());
                    data.setVersionTwo(dependencyFileTwo.getVersion());
                } else if (dependencyFileOne != null && dependencyFileTwo != null && full) {
                    valueAdded = true;
                    data.setVersionOne(dependencyFileOne.getVersion());
                    data.setVersionTwo(dependencyFileTwo.getVersion());
                }

                if (valueAdded) {
                    String[] value = key.split(":");
                    if (!value[0].isEmpty()) {
                        data.setGroupId(value[0]);
                    }
                    if (!value[1].isEmpty()) {
                        data.setArtifactId(value[1]);
                    }
                    dependencyData.add(data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        log.info("Comparison completed");
        return builder.toString().trim().isEmpty() ? "No Result" : builder.toString();
    }

    private void calculateData(String firstFileData, Map<String, Dependency> keyMapFileOne) throws SAXException, IOException, ParserConfigurationException {
        parser.parsePOMXML(dependencyParser, firstFileData);
        parser.getAllData()
                .forEach((object) -> {
                    Dependency dependency = (Dependency) object;
                    keyMapFileOne.put(dependency.getGroupId() + ":" + dependency.getArtifactId(), dependency);
                });
    }

    public String comparePOMFiles(String firstFileData, String secondFileData) {
        return comparePOMFiles(firstFileData, secondFileData, false);
    }
    private static final String NEW_LINE_ONE = "<br/>";
    private static final String NEW_LINE_TWO = "<br/><br/>";
}
