/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.model;

import java.io.Serializable;

/**
 *
 * @author p
 */
public interface POMData extends Serializable {

    String getGroupId();

    String getArtifactId();

    String getVersion();

    void setGroupId(String groupId);

    void setArtifactId(String artifactId);

    void setVersion(String version);
}
