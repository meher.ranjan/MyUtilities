/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.model;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author p
 */
@Getter
@Setter
@ToString
public class Dependency implements POMData {

    private String groupId;
    private String artifactId;
    private String version = "";
    private String scope;
    private boolean exist = true;
}
