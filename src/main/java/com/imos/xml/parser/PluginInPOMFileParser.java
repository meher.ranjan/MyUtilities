/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.xml.parser;

import com.imos.model.Plugin;
import com.alibaba.fastjson.JSON;
import java.util.Arrays;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author p
 */
@Log4j2
public class PluginInPOMFileParser extends BasePOMFileParser<Plugin> {

    private static final String PLUGIN = "plugin";

    private static final List<String> KEYS = Arrays.asList(GROUP_ID, ARTIFACT_ID, VERSION);

    private JSONObject plugin;
    private String parentKey;
    private String key;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (PLUGIN.equals(qName)) {
            plugin = new JSONObject();
            parentKey = PLUGIN;
        } else if (PROPERTIES.equals(qName)) {
            property = true;
        }
        key = qName.trim();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (PLUGIN.equals(parentKey) && KEYS.contains(key)) {
            String value = new String(ch, start, length).trim();
            value = checkValueInProperties(value);
            plugin.put(key, value);
        } else if (property) {
            propertyMap.put(key, new String(ch, start, length).trim());
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (PLUGIN.equals(qName)) {
            data.add(JSON.parseObject(plugin.toString(), Plugin.class));
            parentKey = EMPTY;
        } else if (PROPERTIES.equals(qName)) {
            property = false;
        }
        key = EMPTY;
    }
}
