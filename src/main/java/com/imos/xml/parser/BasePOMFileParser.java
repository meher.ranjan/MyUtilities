/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.xml.parser;

import com.imos.model.POMData;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author p
 * @param <T>
 */
@Log4j2
public class BasePOMFileParser<T> extends DefaultHandler {

    protected static final String PROPERTIES = "properties";
    protected static final String GROUP_ID = "groupId";
    protected static final String ARTIFACT_ID = "artifactId";
    protected static final String VERSION = "version";
    protected boolean property;

    @Getter
    protected Set<POMData> data;

    @Getter
    protected Map<String, String> propertyMap;

    @Override
    public void startDocument() throws SAXException {
        data = new HashSet<>();
        propertyMap = new HashMap<>();
    }

    public void validateAllProperties() {
        data = data.stream()
                .map(d -> {
                    String version = d.getVersion();
                    version = version.isEmpty() ? "Version NA" : checkValueInProperties(version);
                    d.setVersion(version);
                    return d;
                })
                .collect(Collectors.toSet());
        log.info("Validate for all properties value");
    }

    protected String checkValueInProperties(String value) {
        if (value.startsWith("${") && value.endsWith("}")) {
            value = value.replace("${", "");
            value = value.replace("}", "");
        }
        if (propertyMap.containsKey(value)) {
            value = propertyMap.get(value);
        }
        return value;
    }

    protected static final String EMPTY = "";
}
