/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.xml.parser;

import com.imos.model.Dependency;
import com.alibaba.fastjson.JSON;
import java.util.Arrays;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author p
 */
@Log4j2
public class DependencyInPOMFileParser extends BasePOMFileParser<Dependency> {

    private static final String DEPENDENCY = "dependency";
    private static final String SCOPE = "scope";

    private static final List<String> KEYS = Arrays.asList(GROUP_ID, ARTIFACT_ID, VERSION, SCOPE);

    private JSONObject dependency;
    private String parentKey;
    private String key;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (DEPENDENCY.equals(qName)) {
            dependency = new JSONObject();
            parentKey = DEPENDENCY;
        } else if (PROPERTIES.equals(qName)) {
            property = true;
        }
        key = qName.trim();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (DEPENDENCY.equals(parentKey) && KEYS.contains(key)) {
            String value = new String(ch, start, length).trim();
            value = checkValueInProperties(value);
            dependency.put(key, value);
        } else if (property) {
            propertyMap.put(key, new String(ch, start, length).trim());
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (DEPENDENCY.equals(qName)) {
            data.add(JSON.parseObject(dependency.toString(), Dependency.class));
            parentKey = EMPTY;
        } else if (PROPERTIES.equals(qName)) {
            property = false;
        }
        key = EMPTY;
    }
}
