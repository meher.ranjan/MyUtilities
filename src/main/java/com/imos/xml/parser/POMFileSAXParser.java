/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.xml.parser;

import com.imos.model.POMData;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Map;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import lombok.extern.log4j.Log4j2;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author p
 */
@ApplicationScoped
@Log4j2
public class POMFileSAXParser implements Serializable {

    @Inject
    private BasePOMFileParser xmlParser;

    private SAXParser saxParser;

    public POMFileSAXParser() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            saxParser = factory.newSAXParser();
        } catch (ParserConfigurationException | SAXException ex) {
            log.error(ex.getMessage());
        }
    }

    public void parsePOMXML(BasePOMFileParser xmlParser, String xmlData) throws ParserConfigurationException, SAXException, IOException {
        this.xmlParser = xmlParser;
        saxParser.parse(new InputSource(new StringReader(xmlData)), this.xmlParser);
        this.xmlParser.validateAllProperties();
    }

    public <T extends POMData> Set<T> getAllData() {
        return xmlParser.getData();
    }

    public Map<String, String> getAllProperties() {
        return xmlParser.getPropertyMap();
    }
}
