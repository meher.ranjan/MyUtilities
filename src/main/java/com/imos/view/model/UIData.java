/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.view.model;

import java.io.Serializable;

/**
 *
 * @author p
 */
public interface UIData extends Serializable {

    String getGroupId();

    String getArtifactId();

    String getVersionOne();

    String getVersionTwo();
}
