/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.view.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author p
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DependencyData implements UIData {

    private String groupId;
    private String artifactId;
    private String versionOne;
    private String versionTwo;
}
