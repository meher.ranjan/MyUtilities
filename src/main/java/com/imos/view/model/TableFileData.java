/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.view.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author p
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TableFileData implements Serializable {

    private int id;
    private String fileType;
    private String fileName;
}
