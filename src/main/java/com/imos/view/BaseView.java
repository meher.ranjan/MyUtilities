/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.view;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author p
 */
@Log4j2
public abstract class BaseView implements Serializable {

    protected void addInfoMessage(String msgStr) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", msgStr);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    protected void addWarningMessage(String msgStr) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", msgStr);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    protected void addErrorMessage(String msgStr) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msgStr);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    protected void addErrorMessage(String componentId, String msgStr) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", msgStr);
        FacesContext.getCurrentInstance().addMessage(componentId, msg);
    }

    protected void addWarningMessage(String componentId, String msgStr) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", msgStr);
        FacesContext.getCurrentInstance().addMessage(componentId, msg);
    }
}
