/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imos.view;

import com.imos.view.model.TableFileData;
import com.imos.service.xml.POMDependencyCompareService;
import com.imos.service.xml.POMPluginCompareService;
import com.imos.view.model.UIData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author p
 */
@Log4j2
@SessionScoped
@Named("comparePOM")
public class ComparePOMView extends BaseView {

    private List<UploadedFile> uploadedFiles;

    @Getter
    @Setter
    private String compareData = "";

    @Getter
    @Setter
    private String fileOneName = "";
    private String fileOneData = "";

    @Getter
    @Setter
    private String fileOneDataHTML = "";

    @Getter
    @Setter
    private String fileTwoName = "";
    private String fileTwoData = "";

    @Getter
    @Setter
    private String fileTwoDataHTML = "";

    @Inject
    private POMDependencyCompareService dependencyService;

    @Inject
    private POMPluginCompareService pluginService;

    @Getter
    @Setter
    private TableFileData selectedFile;

    @Getter
    @Setter
    private List<TableFileData> fileData;

    @Getter
    @Setter
    private List<UIData> compareDiffData;

    @PostConstruct
    public void config() {
        uploadedFiles = new ArrayList<>();
        fileData = new ArrayList<>();
        compareDiffData = new ArrayList<>();
    }

    public void compareDiffPlugin() {
        BiFunction<String, String, String> func = (first, second) -> {
            return pluginService.comparePOMFiles(first, second);
        };
        boolean status = validation(func, fileOneData, fileTwoData);
        if (status) {
            compareDiffData = new ArrayList<>(pluginService.getPluginData());
            log.info("Two pom.xml files are compared for Plugin Difference");
        }
    }

    public void compareFullDiffPlugin() {
        BiFunction<String, String, String> func = (one, two) -> {
            return pluginService.comparePOMFiles(one, two, true);
        };
        boolean status = validation(func, fileOneData, fileTwoData);
        if (status) {
            compareDiffData = new ArrayList<>(pluginService.getPluginData());
            log.info("Two pom.xml files are compared for Plugin Full Difference");
        }
    }

    public void compareDiffDependency() {
        BiFunction<String, String, String> func = (one, two) -> {
            return dependencyService.comparePOMFiles(one, two);
        };
        boolean status = validation(func, fileOneData, fileTwoData);
        if (status) {
            compareDiffData = new ArrayList<>(dependencyService.getDependencyData());
            log.info("Two pom.xml files are compared for Dependency Difference");
        }
    }

    public void compareFullDiffDependency() {
        BiFunction<String, String, String> func = (one, two) -> {
            return dependencyService.comparePOMFiles(one, two, true);
        };
        boolean status = validation(func, fileOneData, fileTwoData);
        if (status) {
            compareDiffData = new ArrayList<>(dependencyService.getDependencyData());
            log.info("Two pom.xml files are compared for Dependency Full Difference");
        }
    }

    public boolean validation(BiFunction<String, String, String> func, String fileOne, String fileTwo) {
        if (uploadedFiles.isEmpty()) {
            addInfoMessage("Upload two pom.xml file one by one");
            log.info("No files are entered");
            return false;
        } else if (uploadedFiles.size() == 1) {
            addInfoMessage("Enter the second pom.xml file");
            log.info("Only one pom.xml file is entered");
            return false;
        } else if (uploadedFiles.size() == 2) {
            func.apply(fileOne, fileTwo);
            return true;
        } else {
            addInfoMessage("Only two pom.xml files can be compared");
            log.info("Two pom.xml file can be compared, but there are " + uploadedFiles.size());
            return false;
        }
    }

    public void deleteFile() {
        fileData.remove(selectedFile);
        if (selectedFile.getId() == 1) {
            fileOneName = "";
            fileOneData = "";
            fileOneDataHTML = "";
            uploadedFiles.remove(0);
        } else {
            fileTwoName = "";
            fileTwoData = "";
            fileTwoDataHTML = "";
            uploadedFiles.remove(1);
        }
        log.info("Selected file: {} {} is deleted", selectedFile.getFileType(), selectedFile.getFileName());
        selectedFile = null;
    }

    public void clear() {
        compareData = "";
        compareDiffData.clear();
        log.info("Compare Data cleared");
    }

    public void reset() {
        clear();
        fileOneName = fileTwoName = "";
        fileOneDataHTML = fileTwoDataHTML = "";
        fileOneData = fileTwoData = "";
        uploadedFiles.clear();
        fileData.clear();
        log.info("All Data cleared");
    }

    public void handleFileUpload(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        if (fileOneName.isEmpty()) {
            uploadedFiles.add(0, file);
            fileOneName = file.getFileName();
            fileOneData = new String(file.getContents());
            fileData.add(0, new TableFileData(1, "First File", fileOneName));
            fileOneDataHTML = fileOneData;
            fileOneDataHTML = convertIntoHtmlFormat(fileOneDataHTML);
            addInfoMessage(file.getFileName() + " is uploaded.");
        } else if (fileTwoName.isEmpty()) {
            uploadedFiles.add(1, file);
            fileTwoName = file.getFileName();
            fileTwoData = new String(file.getContents());
            fileData.add(1, new TableFileData(2, "Second File", fileTwoName));
            fileTwoDataHTML = fileTwoData;
            fileTwoDataHTML = convertIntoHtmlFormat(fileTwoDataHTML);
            addInfoMessage(file.getFileName() + " is uploaded.");
        } else {
            addWarningMessage("Two pom.xml files are already added.");
        }
        log.info(file.getFileName() + " is uploaded");
    }

    private String convertIntoHtmlFormat(String data) {
        data = data.replaceAll("<", "&lt;");
        data = data.replaceAll(">", "&gt;");
        data = data.replaceAll("\"", "&quot;");
        data = data.replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        data = data.replaceAll("\n", "<br/>");
        return data;
    }
}
